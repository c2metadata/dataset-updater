package us.mtna.dataset.updater.test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import org.ddialliance.ddi_2_5.xml.xmlbeans.CodeBookDocument;
import org.junit.Test;

import us.mtna.dataset.updater.DdiManager;

public class DdiManagerTest {
	
	private void newMerge(File merge1, File merge2, String outputFileName)
			throws FileNotFoundException, UnsupportedEncodingException {
		DdiManager mergeUtil = new DdiManager();
		CodeBookDocument updatedXml = mergeUtil.merge(merge1, merge2);

		PrintWriter writer = new PrintWriter(outputFileName, "UTF-8");
		writer.print(updatedXml);
		writer.close();
	}

	// test new one
	@Test
	public void testNewMerge() throws FileNotFoundException, UnsupportedEncodingException {
		File merge1 = new File("src/test/resources/testFiles/merge1DDI.xml");
		File merge2 = new File("src/test/resources/testFiles/merge2DDI.xml");
		String outputFileName = "src/test/resources/testFiles/newMerged.xml";
		newMerge(merge1, merge2, outputFileName);
	}
}
