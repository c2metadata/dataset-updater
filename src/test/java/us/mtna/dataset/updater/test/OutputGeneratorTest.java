package us.mtna.dataset.updater.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import org.apache.xmlbeans.XmlException;
import org.c2metadata.sdtl.pojo.Program;
import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import us.mtna.dataset.updater.Ddi25OutputGenerator;
import us.mtna.dataset.updater.exception.TransformationException;
import us.mtna.reader.ResourceCopyUtility;
import us.mtna.reader.exceptions.ReaderException;

public class OutputGeneratorTest {

	private ObjectMapper mapper;
	private Ddi25OutputGenerator outputGenerator;

	@Before
	public void setUp() {
		this.outputGenerator = new Ddi25OutputGenerator();
		mapper = new ObjectMapper();
		mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		mapper.findAndRegisterModules();
		ResourceCopyUtility.setObjectMapper(mapper);
	}

	private Program getProgram(File json) {
		try {
			return mapper.readValue(json, Program.class);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	// TODO these may not have the updated sdtl since the files are more often
	// updated in the test project. look at the
	// output generator tests in the ddi output generator test project.

	@Test
	public void testString()
			throws JsonParseException, JsonMappingException, IOException, NoSuchAlgorithmException, XmlException {
		File json = new File("src/test/resources/sdtlJson/spss_rename.json");
		File xml = new File("src/test/resources/inputDdi/renameInputDdi.xml");
		Program program = getProgram(json);
		System.out.println(outputGenerator.getUpdatedXmlAsString(program, new FileInputStream(xml)));
	}

	// Multifile
	@Test
	public void testStringMultiFile() throws NoSuchAlgorithmException, XmlException, IOException {
		File json = new File("src/test/resources/sdtlJson/spssAddFiles.json");
		File xml1 = new File("src/test/resources/inputDdi/merge1DDI.xml");
		File xml2 = new File("src/test/resources/inputDdi/merge2DDI.xml");
		Program program = getProgram(json);
		System.out.println(
				outputGenerator.getUpdatedXmlAsString(program, new FileInputStream(xml1), new FileInputStream(xml2)));
	}

	@Test
	public void testHhincome() throws ReaderException, TransformationException, FileNotFoundException {
		File json = new File("src/test/resources/sdtlJson/hhincomeSdtl.json");
		File xml1 = new File("src/test/resources/inputDdi/hhincomeInputDdi.xml");
		Program program = getProgram(json);
		System.out.println(outputGenerator.getUpdatedXmlAsString(program, new FileInputStream(xml1)));
	}

}
