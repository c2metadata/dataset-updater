/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.dataset.updater;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.c2metadata.sdtl.pojo.FunctionArgument;
import org.c2metadata.sdtl.pojo.expression.AllVariablesExpression;
import org.c2metadata.sdtl.pojo.expression.CompositeVariableNameExpression;
import org.c2metadata.sdtl.pojo.expression.ExpressionBase;
import org.c2metadata.sdtl.pojo.expression.FunctionCallExpression;
import org.c2metadata.sdtl.pojo.expression.GroupedExpression;
import org.c2metadata.sdtl.pojo.expression.MissingValueConstantExpression;
import org.c2metadata.sdtl.pojo.expression.NumericConstantExpression;
import org.c2metadata.sdtl.pojo.expression.StringConstantExpression;
import org.c2metadata.sdtl.pojo.expression.VariableListExpression;
import org.c2metadata.sdtl.pojo.expression.VariableRangeExpression;
import org.c2metadata.sdtl.pojo.expression.VariableReferenceBase;
import org.c2metadata.sdtl.pojo.expression.VariableSymbolExpression;
import org.slf4j.LoggerFactory;

import us.mtna.data.transform.command.CreatesVariables;
import us.mtna.data.transform.command.DeletesVariable;
import us.mtna.data.transform.command.SdtlWrapper;
import us.mtna.data.transform.command.SelectsVariables;
import us.mtna.data.transform.command.UpdatesClassification;
import us.mtna.data.transform.command.UpdatesVariables;
import us.mtna.data.transform.command.ds.AppendsDatasets;
import us.mtna.data.transform.command.ds.LoadsDataset;
import us.mtna.data.transform.command.ds.MergesDatasets;
import us.mtna.data.transform.command.ds.ReordersDataset;
import us.mtna.data.transform.command.ds.SavesDataset;
import us.mtna.data.transform.command.object.ClassificationUpdate;
import us.mtna.data.transform.command.object.ClassificationUtils;
import us.mtna.data.transform.command.object.CodeDetail;
import us.mtna.data.transform.command.object.CodeRangeDetail;
import us.mtna.data.transform.command.object.MinMax;
import us.mtna.data.transform.command.object.NewVariable;
import us.mtna.data.transform.command.object.Range;
import us.mtna.data.transform.command.object.UpdaterMergeFileDescription;
import us.mtna.data.transform.command.object.VariableNamePair;
import us.mtna.data.transform.wrapper.sdtl.Load;
import us.mtna.dataset.updater.exception.MissingSourceVariableException;
import us.mtna.dataset.updater.exception.TransformationException;
import us.mtna.dataset.updater.exception.UnknownDataSetException;
import us.mtna.dataset.updater.impl.DefaultDataSetManagerFactory;
import us.mtna.pojo.Classification;
import us.mtna.pojo.Code;
import us.mtna.pojo.DataSet;
import us.mtna.pojo.DataType;
import us.mtna.pojo.Transform;
import us.mtna.pojo.Variable;
import us.mtna.reader.ResourceCopyUtility;

/**
 * Implement all interfaces on a {@link SdtlWrapper} with the dataset in scope.
 * Ranges are also expanded at this step.
 * 
 * The primary dataset is the one currently in scope, and should be designated
 * at a level above this. If none is designated, the first command should be
 * {@link Load} which will tell the updater which dataset to use.
 * Loading/Saving/storage of datasets is handled in the {@link DataSetManager}.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class DataSetUpdater {

	private DataSet primaryDataset;
	private final DataSetManager dataSetManager;
	private boolean allowComputeClassifications;
	private Map<String, HashSet<String>> possibleCodes;
	private final HashMap<String, Variable> variableMap;
	private org.slf4j.Logger log = LoggerFactory.getLogger(DataSetUpdater.class);
	private static DataSetManagerFactory managerFactory = new DefaultDataSetManagerFactory();

	/**
	 * If users pass in a list of datasets to use, they should specify the one
	 * they want to start working with as the primaryDs. If not, the primaryDs
	 * will be set by looking for a load command at the start of the script.
	 * Failing that, an exception will be thrown if primary ds is still not set
	 * after load and join commands.
	 * 
	 * @param primaryDataset
	 * @param datasets
	 */
	public DataSetUpdater(DataSet primaryDataset, List<DataSet> datasets) {
		this.primaryDataset = primaryDataset;
		this.variableMap = new HashMap<>();
		this.possibleCodes = new HashMap<>();
		this.dataSetManager = managerFactory.getInstance(datasets);
	}

	/**
	 * If only a single dataset is passed in, it is automatically the primary
	 * dataset you will work with.
	 * 
	 * @param primaryDataset
	 */
	public DataSetUpdater(DataSet primaryDataset) {
		this(primaryDataset, Collections.emptyList());
	}

	/**
	 * This will be called once for every individual wrapper object. Each
	 * wrapper will implement one or more interfaces, which hold instructions
	 * for how to manipulate a dataset, variable, or classification. This method
	 * parses the interfaces in the order of ds updates, variable selection,
	 * variable updates, then classification updates.
	 * 
	 * Transforms will be set on variables only in CreatesVariables,
	 * DeletesVariables, and SelectsVariables (after checking that that the
	 * variable does not already contain that transform.)
	 * 
	 * The variable map is cleared at the start of the method for each transform
	 * so that it contains only the variables affected in that transform.
	 * 
	 * @param wrapper
	 * @param transform
	 * @throws TransformationException
	 *             if the transformation cannot be processed for the data set
	 */
	public synchronized DataSet updateDataSet(SdtlWrapper wrapper, Transform transform) {

		for (String property : wrapper.getOriginalCommand().getUnknownProperties()) {
			log.info("Unknown property [" + property + "] found on command "
					+ wrapper.getOriginalCommand().getCommand());
		}
		variableMap.clear();

		if (LoadsDataset.class.isAssignableFrom(wrapper.getClass())) {
			LoadsDataset loads = (LoadsDataset) wrapper;
			primaryDataset = dataSetManager.loadDataSet(loads.getDatasetId());
			log.debug("Loading dataset [" + loads.getDatasetId() + "] and setting as the primary dataset.");
		}
		if (MergesDatasets.class.isAssignableFrom(wrapper.getClass())) {
			MergesDatasets merges = (MergesDatasets) wrapper;
			matchByVariable(merges, transform);
		}
		if (AppendsDatasets.class.isAssignableFrom(wrapper.getClass())) {
			AppendsDatasets appends = (AppendsDatasets) wrapper;
			appendDatasets(appends, transform);
		}
		if (primaryDataset == null) {
			log.info("Primary dataset is null after performing dataset commands and the program cannot continue.");
			throw new UnknownDataSetException(
					"Dataset is null after performing Dataset commands and the program cannot continue.");
		}
		if (CreatesVariables.class.isAssignableFrom(wrapper.getClass())) {
			CreatesVariables creates = (CreatesVariables) wrapper;
			createVariables(creates, transform);
		}
		if (SelectsVariables.class.isAssignableFrom(wrapper.getClass())) {
			getSelectedVariables(wrapper, transform);
		}
		if (ReordersDataset.class.isAssignableFrom(wrapper.getClass())) {
			ReordersDataset reorders = (ReordersDataset) wrapper;
			reorderDataset(reorders, transform);
		}
		// Delete has to follow Selects so that it can use the populated
		// variable map
		if (DeletesVariable.class.isAssignableFrom(wrapper.getClass())) {
			DeletesVariable deletes = (DeletesVariable) wrapper;
			markVariablesAsDeleted(transform, deletes);
		}
		if (UpdatesVariables.class.isAssignableFrom(wrapper.getClass())) {
			UpdatesVariables updatesVar = (UpdatesVariables) wrapper;
			updateVariables(updatesVar);
		}
		if (UpdatesClassification.class.isAssignableFrom(wrapper.getClass())) {
			UpdatesClassification updatesClassif = (UpdatesClassification) wrapper;
			updateClassification(updatesClassif);
		}
		// if (UpdatesDataset.class.isAssignableFrom(wrapper.getClass())) {
		// UpdatesDataset updatesVar = (UpdatesDataset) wrapper;
		// updateDataset(updatesVar);
		// }
		if (SavesDataset.class.isAssignableFrom(wrapper.getClass())) {
			SavesDataset saves = (SavesDataset) wrapper;
			saveDataset(saves, transform);
		}
		return primaryDataset;
	}

	/**
	 * Reorder the variables in a dataset and sets the transform on the dataset
	 * 
	 * @param reordersDataset
	 * @param transform
	 */
	private void reorderDataset(ReordersDataset reordersDataset, Transform transform) {
		LinkedHashSet<String> variableOrder = new LinkedHashSet<>();
		for (VariableReferenceBase base : reordersDataset.getVariableOrder()) {
			variableOrder.addAll(parseExpressionBase(base, primaryDataset.getMetadata().getVarNameMap()));
		}
		ArrayList<Variable> newOrder = new ArrayList<>();
		log.trace("Reordering dataset with command [" + transform.getOriginalCommand().getCommand() + "]");
		for (String variableName : variableOrder) {
			Variable variable = variableMap.get(variableName);
			newOrder.add(variable);
		}
		if (!newOrder.isEmpty()) {
			primaryDataset.getMetadata().setVariables(newOrder);
		}
		primaryDataset.addCommands(transform);
	}

	/**
	 * saves a copy of the dataset in the datasetManager and adds the transform
	 * to the dataset
	 * 
	 * @param savesDataset
	 * @param transform
	 */
	private void saveDataset(SavesDataset savesDataset, Transform transform) {
		log.trace("Saving dataset with ID [" + savesDataset.getDatasetId() + "]");
		primaryDataset.addCommands(transform);
		dataSetManager.saveDataSet(primaryDataset);
	}

	/**
	 * Merges datasets by just appending the variables.
	 * 
	 * @param appendsDatasets
	 * @param transform
	 */
	private void appendDatasets(AppendsDatasets appendsDatasets, Transform transform) {
		LinkedHashMap<String, Variable> orderedVarMap = new LinkedHashMap<>();
		DataSet newDataSet = new DataSet();
		newDataSet.addCommands(transform);
		for (String datasetId : appendsDatasets.getFileNames()) {
			DataSet dataset = dataSetManager.loadDataSet(datasetId);
			orderedVarMap.putAll(dataset.getMetadata().getVarNameMap());
			log.info("Appending variables from dataset [id=" + datasetId + "] into target dataset ["
					+ newDataSet.getId() + "]");
		}
		newDataSet.getMetadata().setVariables(new LinkedList<Variable>(orderedVarMap.values()));
		setPrimaryDataset(newDataSet);
	}

	/**
	 * performs a match by merge by going through all of the properties
	 * specified in the merge file descriptions first, then doing the merge
	 * command level updates. Note that the first and last variables that are in
	 * the merge command (just variables added to the end of the dataset) are
	 * handled by the createsVariables interface.
	 * 
	 * this creates a list of variables and sets them as the metadata on the
	 * dataset.
	 * 
	 * @param merges
	 * @param transform
	 * @return a linked hash map of the newly merged variable list that
	 */
	private void matchByVariable(MergesDatasets merges, Transform transform) {
		DataSet ds = new DataSet();
		//moved addCommands to end of method since it wasn't getting passed on to newly assigned ds

		Map<String, LinkedHashSet<Variable>> datasetVars = new HashMap<>();
		for (UpdaterMergeFileDescription file : merges.getMergeFileDscr()) {
			// log.info("Merging dataset [id=" + datasetId + "] into target
			// dataset [" + newDataSet.getId() + "]");
			LinkedHashSet<Variable> vars = new LinkedHashSet<>();
			if (file.getFileName() != null) {
				ds = dataSetManager.loadDataSet(file.getFileName());
				for (Variable v : ds.getMetadata().getVarNameMap().values()) {
					vars.add(v);
				}

				if (file.getMergeFlagVariable() != null) {
					Variable v = new Variable();
					v.addTransforms(transform);
					v.setName(file.getMergeFlagVariable());
					// TODO look into what else should be set here - classif
					// info
					vars.add(v);
				}
				datasetVars.put(ds.getId(), vars);

				if (file.getRenameVariables() != null && !file.getRenameVariables().isEmpty()) {
					for (VariableNamePair pair : file.getRenameVariables()) {
						if (pair.getSource() != null && pair.getTarget() != null) {
							if (ds.getMetadata().getVarNameMap().get(pair.getSource()) != null) {
								Variable varToRename = ds.getMetadata().getVarNameMap().get(pair.getSource());
								varToRename.setName(pair.getTarget());
								// TODO make sure this gets into the dataset
								// vars. could either use linked hash map, or do
								// this
								// before adding them
							}
						}
					}
				}
				keepAndDropVars(file.getKeepVariables(), file.getDropVariables(), transform,
						ds.getMetadata().getVarNameMap());
			}
		}
		// This uses the last dataset that was parsed but the merge by vars
		// should still be in there
		LinkedHashSet<String> matchByVars = parseExpressionBase(merges.getMergeByVariables(),
				ds.getMetadata().getVarNameMap());

		LinkedHashMap<String, Variable> finalList = new LinkedHashMap<>();
		// add all vars in the first set
		// add all but those in the set for the second to nth datasets
		for (Entry<String, LinkedHashSet<Variable>> entry : datasetVars.entrySet()) {
			for (Variable v : entry.getValue()) {
				if (finalList.keySet().contains(v.getName())) {
					if (!matchByVars.contains(v.getName())) {
						// TODO implement overlap to determine what to do here.
						// if its overlap,
						log.warn("Duplicate variable name [" + v.getName()
								+ "] found when merging datasets. The duplicate variable was added.");
						finalList.put(v.getName(), v);
					}
				} else {
					finalList.put(v.getName(), v);
				}
			}
		}
		keepAndDropVars(merges.getKeepVariables(), merges.getDropVariables(), transform, finalList);
		ds.getMetadata().setVariables(new LinkedList<Variable>(finalList.values()));
		ds.addCommands(transform);
	}

	private void keepAndDropVars(VariableReferenceBase[] keeps, VariableReferenceBase[] drops, Transform transform,
			Map<String, Variable> variableMap) {
		LinkedHashSet<String> keepVars = new LinkedHashSet<>();
		LinkedHashSet<String> dropVars = new LinkedHashSet<>();
		if (keeps != null) {
			for (VariableReferenceBase keep : keeps) {
				keepVars.addAll(parseExpressionBase(keep, variableMap));
			}
		}
		if (drops != null) {
			for (VariableReferenceBase drop : drops) {
				dropVars.addAll(parseExpressionBase(drop, variableMap));
			}
		}
		if (!keepVars.isEmpty()) {
			for (Entry<String, Variable> entry : variableMap.entrySet()) {
				if (!keepVars.contains(entry.getKey())) {
					entry.getValue().setDeleted(true);
					entry.getValue().addTransforms(transform);
				}
			}
		}

		if (!dropVars.isEmpty()) {
			// mark as deleted but keep them in the metadata
			for (String s : dropVars) {
				if (variableMap.get(s) != null) {
					Variable v = variableMap.get(s);
					v.setDeleted(true);
					v.addTransforms(transform);
				}
			}
		}
	}

	/**
	 * get the vars from any wrapper implementing {@link SelectsVariables}
	 * interface and puts them in the global variableMap. These are the
	 * variables that will be operated on for the current command only.
	 * 
	 */
	private void getSelectedVariables(SdtlWrapper wrapper, Transform transform) {
		SelectsVariables selector = (SelectsVariables) wrapper;
		Map<String, Variable> datasetVariableMap = primaryDataset.getMetadata().getVarNameMap();

		if (!selector.getRanges().isEmpty()) {
			// extractRangeVariables(transform, selector.getRanges(),
			// datasetVariableMap);
			datasetVariableMap
					.putAll(extractAndReturnRangeVariables(transform, selector.getRanges(), datasetVariableMap));
		}
		// if there's a variable list, add the transform to each if it's
		// not already there.
		if (selector.getVariables() != null) {
			for (String variableName : selector.getVariables()) {
				if (datasetVariableMap.get(variableName) != null) {
					if (!new ArrayList<Transform>(Arrays.asList(datasetVariableMap.get(variableName).getTransforms()))
							.contains(transform)) {
						datasetVariableMap.get(variableName).addTransforms(transform);
					}
					log.trace("Adding variable [name=" + variableName + "] to the variable list");
					variableMap.put(variableName, datasetVariableMap.get(variableName));
				}
			}
		}
	}

	/**
	 * Expand and return a list of range objects
	 * 
	 * @param transform
	 * @param ranges
	 *            list of ranges to expand
	 * @param datasetVariableMap
	 *            - this is the current map of variables from the primary
	 *            dataset.
	 */
	private HashMap<String, Variable> extractAndReturnRangeVariables(Transform transform, List<Range> ranges,
			Map<String, Variable> datasetVariableMap) {
		HashMap<String, Variable> variables = new HashMap<>();
		for (Range range : ranges) {
			// check if it's actually a range or just has a "first" value
			if (range.getStart() != null) {
				if (range.getEnd() != null) {
					// determined to have a range, add the transform if it's
					// not already there
					for (Variable variable : primaryDataset.getVariablesInRange(range.getStart(), range.getEnd())) {
						if (!new ArrayList<Transform>(Arrays.asList(variable.getTransforms())).contains(transform)) {
							variable.addTransforms(transform);
						}
						variables.put(variable.getName(), variable);
					}
					// else if the end value is null, you're only dealing
					// with a start value.
				} else {
					if (!new ArrayList<Transform>(
							Arrays.asList(datasetVariableMap.get(range.getStart()).getTransforms()))
									.contains(transform)) {
						datasetVariableMap.get(range.getStart()).addTransforms(transform);
					}
					variables.put(range.getStart(), datasetVariableMap.get(range.getStart()));
				}
			}
		}
		return variables;
	}

	/**
	 * Create a variable as either a copy of a source variable or new, then add
	 * the transform to it, and register it in the dataset
	 * 
	 * @param createsVariables
	 * @param transform
	 */
	private void createVariables(CreatesVariables createsVariables, Transform transform) {
		for (NewVariable newVariable : createsVariables.getNewVariables()) {
			// see if basis var name is null which will determine if you need a
			// new one or a reference.
			Variable variable;
			if (allowComputeClassifications) {
				addPossibleCodes(newVariable);
			}
			if (newVariable.getBasisVariableName() != null) {
				variable = createCopyOfBasisVariable(transform, newVariable);
			} else if (primaryDataset.getMetadata().getVarNameMap().containsKey(newVariable.getNewVariableName())) {
				// // if the new var name is already in the metadata and that's
				// allowed, just use a copy of that one
				populateVariableFromMetadata(transform, newVariable);
				continue;
			} else {
				variable = createNewVariable(transform, newVariable);
			}
			variable.addTransforms(transform);
			variable.setName(newVariable.getNewVariableName());
			primaryDataset.getMetadata().getVariables().add(variable);
			primaryDataset.getMetadata().getVarNameMap().put(variable.getName(), variable);
			variableMap.put(variable.getName(), variable);
			log.trace("Registering variable [id=" + variable.getId() + "] in the metadata.");
		}
	}

	private void addPossibleCodes(NewVariable newVariable) {
		HashSet<String> newVariableCodes = new HashSet<>();
		for (String possibleCode : newVariable.getPossibleCodes()) {
			newVariableCodes.add(possibleCode);
		}
		if (possibleCodes.keySet().contains(newVariable.getNewVariableName())) {
			HashSet<String> existingCodes = possibleCodes.get(newVariable.getNewVariableName());
			existingCodes.addAll(newVariableCodes);
			possibleCodes.put(newVariable.getNewVariableName(), existingCodes);
		} else {
			possibleCodes.put(newVariable.getNewVariableName(), newVariableCodes);
		}
	}

	private Variable createNewVariable(Transform transform, NewVariable newVariable) {
		Variable variable = new Variable();
		variable.setClassificationId(UUID.randomUUID().toString());
		primaryDataset.getMetadata().getClassifs().put(variable.getClassificationId(), new Classification());
		addSourceVariablesFromRange(transform, newVariable);
		transform.addSourceIds(newVariable.getSourceVariables().toArray(new String[0]));
		log.trace("Creating new variable [id=" + variable.getId()
				+ "] and registering its new classification in the metadata.");
		return variable;
	}

	private Variable populateVariableFromMetadata(Transform transform, NewVariable newVariable) {
		Variable variable;
		variable = primaryDataset.getMetadata().getVarNameMap().get(newVariable.getNewVariableName());
		checkForNullSourceVariable(transform, newVariable.getNewVariableName(), variable);
		addSourceVariablesFromRange(transform, newVariable);
		transform.addSourceIds(
				primaryDataset.getMetadata().getVarNameMap().get(newVariable.getNewVariableName()).getId());
		variable.removeAllTransforms();
		variable.addTransforms(transform);
		variable.setId(UUID.randomUUID().toString());
		variable.setName(newVariable.getNewVariableName());
		log.trace("Creating new variable with name [" + newVariable.getNewVariableName()
				+ "]. A variable with the same name was found in the metadata, so a copy of that variable is being used as the basis");
		return variable;
	}

	private void addSourceVariablesFromRange(Transform transform, NewVariable newVariable) {
		if (newVariable.getSourceVariableRange() != null) {
			Range range = newVariable.getSourceVariableRange();
			List<Variable> sourceVarsFromRange = primaryDataset.getVariablesInRange(range.getStart(), range.getEnd());
			for (Variable variable : sourceVarsFromRange) {
				transform.addSourceIds(variable.getName());
			}
		}
	}

	private Variable createCopyOfBasisVariable(Transform transform, NewVariable newVariable) {
		Variable originalVariable = primaryDataset.getMetadata().getVarNameMap()
				.get(newVariable.getBasisVariableName());
		Variable variableCopy = ResourceCopyUtility.copyResource(Variable.class,
				primaryDataset.getMetadata().getVarNameMap().get(newVariable.getBasisVariableName()));
		checkForNullSourceVariable(transform, newVariable.getBasisVariableName(), variableCopy);
		variableCopy.setName(newVariable.getNewVariableName());
		variableCopy.setClassificationId(originalVariable.getClassificationId());
		// remove transforms and set new id so you don't get old
		// derivations on this var.
		variableCopy.removeAllTransforms();
		variableCopy.setId(UUID.randomUUID().toString());
		transform.addSourceIds(originalVariable.getId());
		log.trace("Creating a variable based on variable [name=" + newVariable.getBasisVariableName()
				+ "] from the datset with properties from command [" + transform.getOriginalCommand().getCommand()
				+ "]");
		return variableCopy;
	}

	private void checkForNullSourceVariable(Transform transform, String name, Variable variable) {
		if (variable == null) {
			log.warn("Variable [" + name + "] is null, cannot continue with recode");
			throw new MissingSourceVariableException("Variable [" + name + "] is null, cannot continue with recode.",
					name, transform.getOriginalCommand().getCommand());
		}
	}

	/**
	 * marks variables as deleted and adds the transform to the variable.
	 * 
	 * @param transform
	 * @param deletes
	 */
	private void markVariablesAsDeleted(Transform transform, DeletesVariable deletes) {
		boolean listEmpty = false;
		boolean rangeEmpty = false;
		if (deletes.getDeletedVars() == null || deletes.getDeletedVars().isEmpty()) {
			listEmpty = true;
		}
		if (deletes.getDeletedVariableRanges() == null || deletes.getDeletedVariableRanges().isEmpty()) {
			rangeEmpty = true;
		}
		if (listEmpty && rangeEmpty) {
			log.debug("The list of variables to be deleted is empty.");
			return;
		}
		// get check for ranges of deleted variables
		Map<String, Variable> datasetVariableMap = primaryDataset.getMetadata().getVarNameMap();
		datasetVariableMap.putAll(
				extractAndReturnRangeVariables(transform, deletes.getDeletedVariableRanges(), datasetVariableMap));

		for (String deletedVariable : deletes.getDeletedVars()) {
			Variable variable = variableMap.get(deletedVariable);
			if (variable != null) {
				variable.setDeleted(true);
				log.trace("Setting variable [name=" + variable.getName() + "] as deleted.");
			} else {
				log.debug("Attempted to delete variable [name=" + deletedVariable + "] but it could not be found.");
			}
		}
	}

	/**
	 * updates the variable name and label if available.
	 * 
	 * @param updatesVariables
	 */
	private void updateVariables(UpdatesVariables updatesVariables) {
		for (Variable variable : variableMap.values()) {
			if (updatesVariables.getUpdatedVariables() != null) {
				for (VariableNamePair pair : updatesVariables.getUpdatedVariables()) {
					if (variable.getName().equals(pair.getSource())) {
						variable.setName(pair.getTarget());
						if (pair.getLabel() != null) {
							variable.setLabel(pair.getLabel());
						}
					}
				}
			} else {
				log.debug("The list of variables to update is null.");
			}
		}
	}

	/**
	 * Do any classification updating needed: recode, set value labels, etc.
	 * Each time a new classification will be created by the information in the
	 * UpdatesClassificaiton object.
	 * 
	 * @param transform
	 * @param updateClassification
	 */
	private void updateClassification(UpdatesClassification updateClassification) {
		ClassificationUpdate update = updateClassification.getUpdate();
		List<String> usedCodes = new ArrayList<>();

		for (Variable variable : variableMap.values()) {
			if (variable == null) {
				variable = new Variable();
			}

			Classification classification = determineClassificationToUse(updateClassification, variable);
			checkForDataTypeChange(variable, classification);

			removeCodes(update, usedCodes, classification);
			updateCodes(update, usedCodes, classification);
			addCodes(update, usedCodes, classification);

			// if not carrying over floating codes, remove all but the codes
			// you've touched from the original code list.
			if (!updateClassification.copyFloatingCodes()) {
				List<Code> untouched = ClassificationUtils.getUntouchedCodes(classification, usedCodes);
				for (Code code : untouched) {
					classification.removeCode(code.getCodeValue());
					log.trace("UpdatesClassification: Removing unused code [" + code.getCodeValue()
							+ "] from classification [" + classification.getId()
							+ "]. (Floating codes are set to not be carried over.)");
				}
			}
		}
	}

	/**
	 * A recode can change a variable's dataType: determine if new codes are
	 * numeric or strings. assuming all codes are of the same type
	 * 
	 * @param variable
	 * @param classification
	 */
	private void checkForDataTypeChange(Variable variable, Classification classification) {

		boolean numericCodes = false;
		if (!classification.getCodeList().isEmpty()
				&& org.apache.commons.lang3.StringUtils.isNumeric(classification.getCodeList().get(0).getCodeValue())) {
			numericCodes = true;
		}
		DataType variableType = variable.getDataType();
		if (variableType == DataType.STRING && numericCodes) {
			variable.setDataType(DataType.NUMBER);
		} else if (variableType == DataType.NUMBER && !numericCodes) {
			variable.setDataType(DataType.STRING);
		}
	}

	/**
	 * determines if the classification should be a new classification copied
	 * from one in the metatada, or if you can edit that classification directly
	 * 
	 * @param updatesClassification
	 * @param variable
	 * @return Classification to use for the rest of the updates
	 */
	private Classification determineClassificationToUse(UpdatesClassification updatesClassification,
			Variable variable) {
		Classification classification;
		if (updatesClassification.requiresCopyOfClassification()) {
			if (!possibleCodes.keySet().contains(variable.getName())) {
				Classification oldClassif = primaryDataset.getMetadata()
						.lookupClassificationById(variable.getClassificationId()) == null ? new Classification()
								: primaryDataset.getMetadata().lookupClassificationById(variable.getClassificationId());
				classification = ResourceCopyUtility.copyResource(Classification.class, oldClassif);
				classification.setId(UUID.randomUUID().toString());
				primaryDataset.getMetadata().getClassifs().put(classification.getId(), classification);
				variable.setClassification(classification);
				log.trace("UpdatesClassification requires a new classification, so new classification ["
						+ classification.getId() + "] was created and registered in the metadata .");
			} else {
				// check the primary ds metadata first to see if you'll just
				// need to add more codes to that one. If its not there, just
				// create a new classif to populate
				classification = primaryDataset.getMetadata().getClassifs().keySet()
						.contains(variable.getClassificationId())
								? primaryDataset.getMetadata().getClassifs().get(variable.getClassificationId())
								: new Classification();

				// if there are possible codes associated with the classifName,
				// add the codes to the classif
				if (possibleCodes.get(variable.getName()) != null) {
					HashSet<String> codesToUse = possibleCodes.get(variable.getName());
					List<Code> codesToAdd = new ArrayList<>();
					for (String codeValue : codesToUse) {
						Code code = new Code();
						code.setCodeValue(codeValue);
						codesToAdd.add(code);
					}
					classification.addCodesToCodeList(codesToAdd);
				}
				classification.setId(UUID.randomUUID().toString());
				variable.setClassificationId(classification.getId());
				variable.setClassification(classification);
				primaryDataset.getMetadata().getClassifs().put(classification.getId(), classification);
			}
		} else {// else you may modify the classification from the metadata
				// directly.
			classification = primaryDataset.getMetadata()
					.lookupClassificationById(variable.getClassificationId()) == null ? new Classification()
							: primaryDataset.getMetadata().lookupClassificationById(variable.getClassificationId());
			log.trace("New classification not required for UpdatesClassification, so classification ["
					+ classification.getId() + "] pulled from the metadata to be updated.");
		}
		return classification;
	}

	/**
	 * go through and lookup codes by string, then use code mapping utility to
	 * look them up by numeric value to see if there a match that way
	 * 
	 * @param classificationUpdate
	 * @param usedCodes
	 * @param classification
	 */
	private void updateCodes(ClassificationUpdate classificationUpdate, List<String> usedCodes,
			Classification classification) {
		if (classificationUpdate.getUpdatesCodes() != null) {
			for (CodeDetail codeDetail : classificationUpdate.getUpdatesCodes()) {
				// if from value not contained in the classification, see if
				// it's because it was created by a compute command.
				if (!classification.getCodeList().contains(codeDetail.getFromValue())) {
					// FIXME: take out this check? just create it anyway?
					if (allowComputeClassifications && possibleCodes.containsKey(codeDetail.getFromValue())) {
						classification.addNewCode(createCode(codeDetail));
					}

					if (codeDetail.getFromValue() == null) {
						// if there is no from value, you are adding a code from
						// nothing.
						classification.addNewCode(createCode(codeDetail));
					}
				}

				// get the matching code already on the classification
				Code code = classification.lookupCode(codeDetail.getFromValue());
				if (code == null) {
					// lookup by numeric
					log.trace("UpdatesClassification: no match found when looking up code [" + codeDetail.getFromValue()
							+ "] in classification [" + classification.getId()
							+ "]. Looking up code value numerically");
					CodeMappingUtility mappingUtility = CodeMappingUtility.getInstance(classification);
					List<Code> numericallyMatchedCodes = mappingUtility.lookupNumericCode(codeDetail.getFromValue());
					// update all of the codes found to be a numeric match.
					for (Code c : numericallyMatchedCodes) {
						log.trace("UpdatesClassification: updating code [" + c.getCodeValue() + "].");
						updateCode(c, codeDetail.getLabel(), codeDetail.isMissing(), codeDetail.getMissingType());
						usedCodes.add(c.getCodeValue());
					}
				} else {
					log.trace("UpdatesClassification: updating code [" + code.getCodeValue() + "].");
					updateCode(code, codeDetail.getLabel(), codeDetail.isMissing(), codeDetail.getMissingType());
					usedCodes.add(code.getCodeValue());
				}
			}
		}
		if (classificationUpdate.getUpdatesCodeRange() != null) {
			for (CodeRangeDetail rangeDetail : classificationUpdate.getUpdatesCodeRange()) {
				for (Code code : classification.selectCodeRange(rangeDetail.getFromRange().getStart(),
						rangeDetail.getFromRange().getEnd())) {
					log.trace("UpdatesClassification: updating code [" + code.getCodeValue() + "].");
					updateCode(code, rangeDetail.getLabel(), rangeDetail.isMissing(), rangeDetail.getMissingType());
					usedCodes.add(code.getCodeValue());
				}
			}
		}
		if (classificationUpdate.getUpdatesCodes() != null && classificationUpdate.getUpdatesCodeRange() != null) {
			log.trace("UpdatesClassification: no codes found to update.");
		}
	}

	/**
	 * remove specified codes from a classification, and add them to the list of
	 * used codes
	 * 
	 * @param classificationUpdate
	 * @param usedCodes
	 * @param classification
	 */
	private void removeCodes(ClassificationUpdate classificationUpdate, List<String> usedCodes,
			Classification classification) {
		if (classificationUpdate.getRemovedCodes() != null) {
			for (String codeValue : classificationUpdate.getRemovedCodes()) {
				classification.removeCode(codeValue);
				usedCodes.add(codeValue);
				log.trace("UpdatesClassification: removing code [" + codeValue + "].");
			}
		}
		if (classificationUpdate.getRemovedCodeRanges() != null) {
			for (Range range : classificationUpdate.getRemovedCodeRanges()) {
				if (range.getEnd().equals(MinMax.MAXIMUM.getValue())) {
					if (!classification.getCodeList().isEmpty()) {
						// set to last code
						range.setEnd(classification.getCodeList().get((classification.getCodeList().size() - 1))
								.getCodeValue());
					}
				}
				if (range.getStart().equals(MinMax.MINIMUM.getValue())) {
					if (!classification.getCodeList().isEmpty()) {
						// set to first code
						range.setStart(classification.getCodeList().get(0).getCodeValue());
					}

				}
				for (Code code : classification.selectCodeRange(range.getStart(), range.getEnd())) {
					classification.removeCode(code.getCodeValue());
					usedCodes.add(code.getCodeValue().trim());
					log.trace("UpdatesClassification: removing code [" + code.getCodeValue() + "].");
				}
			}
		}
		if (classificationUpdate.getRemovedCodeRanges() == null && classificationUpdate.getRemovedCodes() == null) {
			log.trace("UpdatesClassification: no codes found to remove.");
		}
	}

	/**
	 * Adds specified codes to a classification.
	 * 
	 * @param update
	 * @param classification
	 */
	private void addCodes(ClassificationUpdate update, List<String> usedCodes, Classification classification) {
		if (update.getNewCodes() != null) {
			for (CodeDetail codeDetail : update.getNewCodes()) {
				Code code = createCode(codeDetail);
				classification.addNewCode(code);
				usedCodes.add(code.getCodeValue());
				log.trace("UpdatesClassification: creating code [" + code.getCodeValue() + "].");
			}
		}
		if (update.getNewCodeRanges() != null) {
			for (CodeRangeDetail rangeDetail : update.getNewCodeRanges()) {
				Code code = createCode(
						new CodeDetail(rangeDetail.getTargetValue(), rangeDetail.getLabel(), rangeDetail.isMissing()));
				classification.addNewCode(code);
				usedCodes.add(code.getCodeValue());
				log.trace("UpdatesClassification: creating code [" + code.getCodeValue() + "].");
			}
		}
		if (update.getNewCodes() == null && update.getNewCodeRanges() == null) {
			log.trace("UpdatesClassification: no new codes created.");
		}
	}

	/**
	 * Updates a code's label and missing status
	 * 
	 * @param code
	 * @param label
	 * @param missing
	 * @param missingType
	 */
	private void updateCode(Code code, String label, boolean missing, String missingType) {
		code.setLabel(label);
		code.setMissing(missing);
	}

	/**
	 * Creates a new code from the code detail
	 * 
	 * @param codeDetail
	 * @return new populated code
	 */
	private Code createCode(CodeDetail codeDetail) {
		Code code = new Code();
		if (codeDetail.getNewValue() != null) {
			code.setCodeValue(codeDetail.getNewValue());
		}
		if (codeDetail.getLabel() != null) {
			code.setLabel(codeDetail.getLabel());
		}
		code.setMissing(codeDetail.isMissing());
		return code;
	}

	public org.slf4j.Logger getLog() {
		return log;
	}

	public void setLog(org.slf4j.Logger log) {
		this.log = log;
	}

	public static void setDataSetManagerFactory(DataSetManagerFactory managerFactory) {
		DataSetUpdater.managerFactory = managerFactory;
	}

	public void setPrimaryDataset(DataSet dataset) {
		this.primaryDataset = dataset;
	}

	public DataSet getPrimaryDataset() {
		return primaryDataset;
	}

	/**
	 * checks if scripts are allowed to create classifications with a compute
	 * command that has a numeric value. false: not allowed.
	 * 
	 * @return
	 */
	public boolean isAllowComputeClassifications() {
		return allowComputeClassifications;
	}

	/**
	 * set this flag to true to allow classifications to be created by compute
	 * statements, if that classification is later referred to
	 * 
	 * @param allowComputeClassifications
	 */
	public void setAllowComputeClassifications(boolean allowComputeClassifications) {
		this.allowComputeClassifications = allowComputeClassifications;
	}

	public Map<String, HashSet<String>> getPossibleCodes() {
		return possibleCodes;
	}

	public void setPossibleCodes(Map<String, HashSet<String>> possibleCodes) {
		this.possibleCodes = possibleCodes;
	}

	public DataSetManager getDataSetManager() {
		return dataSetManager;
	}

	// TODO maybe replace this with the parseExpressionBase - the only
	// difference is this one uses the extract and return range variables method
	// instead of the one straight from the dataset to set the transform on the
	// newly gotten variables.
	// private HashSet<String> parseVarReferenceBaseWithDataset(Transform
	// transform, VariableReferenceBase referenceBase,
	// Map<String, Variable> datasetVars) {
	// HashSet<String> vars = new HashSet<>();
	// if (referenceBase != null) {
	// if
	// (VariableListExpression.class.isAssignableFrom(referenceBase.getClass()))
	// {
	// VariableListExpression list = (VariableListExpression) referenceBase;
	// for (VariableReferenceBase ref : list.getVariables()) {
	// vars.addAll(parseVarReferenceBaseWithDataset(transform, ref,
	// datasetVars));
	// }
	// } else if
	// (VariableRangeExpression.class.isAssignableFrom(referenceBase.getClass()))
	// {
	// VariableRangeExpression range = (VariableRangeExpression) referenceBase;
	// vars.addAll(extractAndReturnRangeVariables(transform, range,
	// datasetVars));
	//
	// } else if
	// (CompositeVariableNameExpression.class.isAssignableFrom(referenceBase.getClass()))
	// {
	// CompositeVariableNameExpression composite =
	// (CompositeVariableNameExpression) referenceBase;
	// // TODO - do need the dataset for this
	// // vars.addAll(parseExpressionBase(composite.getComputedVariableName(),
	// // datasetVars));
	// } else if
	// (AllVariablesExpression.class.isAssignableFrom(referenceBase.getClass()))
	// {
	// vars.addAll(datasetVars.keySet());
	// } else if
	// (VariableSymbolExpression.class.isAssignableFrom(referenceBase.getClass()))
	// {
	// VariableSymbolExpression symbol = (VariableSymbolExpression)
	// referenceBase;
	// vars.add(symbol.getVariableName());
	// }
	// }
	// return vars;
	// }

	// utlity to parse expression bases that may hold variable names
	private LinkedHashSet<String> parseExpressionBase(ExpressionBase expression, Map<String, Variable> varMap) {
		LinkedHashSet<String> vars = new LinkedHashSet<>();
		if (expression != null) {

			if (VariableSymbolExpression.class.isAssignableFrom(expression.getClass())) {
				VariableSymbolExpression expr = (VariableSymbolExpression) expression;
				vars.add(expr.getVariableName());
			} else if (VariableRangeExpression.class.isAssignableFrom(expression.getClass())) {
				VariableRangeExpression expr = (VariableRangeExpression) expression;
				List<Variable> vasr = getVariablesInRange(expr.getFirst(), expr.getLast(), varMap);
				for (Variable v : vasr) {
					vars.add(v.getName());
				}
			} else if (VariableListExpression.class.isAssignableFrom(expression.getClass())) {
				VariableListExpression expr = (VariableListExpression) expression;
				for (VariableReferenceBase base : expr.getVariables()) {
					vars.addAll(parseExpressionBase(base, varMap));
				}
			} else if (FunctionCallExpression.class.isAssignableFrom(expression.getClass())) {
				FunctionCallExpression expr = (FunctionCallExpression) expression;
				for (FunctionArgument eb : expr.getArguments()) {
					vars.addAll(parseExpressionBase(eb.getArgumentValue(), varMap));
				}
			} else if (GroupedExpression.class.isAssignableFrom(expression.getClass())) {
				GroupedExpression expr = (GroupedExpression) expression;
				vars.addAll(parseExpressionBase(expr.getExpression(), varMap));
			} else if (NumericConstantExpression.class.isAssignableFrom(expression.getClass())) {
				NumericConstantExpression expr = (NumericConstantExpression) expression;
				vars.add(String.valueOf(expr.getValue()));
			} else if (MissingValueConstantExpression.class.isAssignableFrom(expression.getClass())) {
				MissingValueConstantExpression expr = (MissingValueConstantExpression) expression;
				vars.add(String.valueOf(expr.getValue()));
			} else if (StringConstantExpression.class.isAssignableFrom(expression.getClass())) {
				StringConstantExpression expr = (StringConstantExpression) expression;
				vars.add(expr.getValue());
			} else if (CompositeVariableNameExpression.class.isAssignableFrom(expression.getClass())) {
				CompositeVariableNameExpression composite = (CompositeVariableNameExpression) expression;
				vars.addAll(parseExpressionBase(composite.getComputedVariableName(), varMap));
			} else if (AllVariablesExpression.class.isAssignableFrom(expression.getClass())) {
				vars.addAll(varMap.keySet());
			}
		}
		return vars;
	}

	public List<Variable> getVariablesInRange(String from, String to, Map<String, Variable> originalMap) {

		List<Variable> vars = new ArrayList<>();

		Map<String, Variable> varMap = new HashMap<>();
		for (Entry<String, Variable> entry : originalMap.entrySet()) {
			varMap.put(entry.getKey().toUpperCase(), entry.getValue());
		}

		// find out if I can sort on start position or if I have to do it
		// by natural order and width. if even one doesn't have a start position
		// you can't use this
		boolean useStartPosition = true;
		for (Variable variable : varMap.values()) {
			if (variable.getStartPosition() == null) {
				useStartPosition = false;
				break;
			}
		}

		// sort if start position. if useStartPosition = false, they will
		// already be sorted by natural order
		List<Variable> sortedVars = new ArrayList<>(originalMap.values());
		if (useStartPosition) {
			Collections.sort(sortedVars, Variable.StartPositionComparator);
		}

		Variable mapFirst = varMap.get(from.toUpperCase());
		Variable mapLast = varMap.get(to.toUpperCase());
		int toIndex = sortedVars.indexOf(mapLast);
		int fromIndex = sortedVars.indexOf(mapFirst);

		// use indices to get the vars in the range (+1 on end index since it is
		// exclusive)
		List<Variable> subList = sortedVars.subList(fromIndex, toIndex + 1);

		for (Variable variable : subList) {
			vars.add(variable);
		}
		return vars;
	}

}
