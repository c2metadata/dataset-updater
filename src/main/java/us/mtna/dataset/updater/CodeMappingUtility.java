/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.dataset.updater;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.math.NumberUtils;

import us.mtna.pojo.Classification;
import us.mtna.pojo.Code;

/**
 * Resource for matching codes numerically
 * 
 * Use case: suppose you have a classification with this list of codes: 1, 2, 3.
 * Looking up 1.00 in this list will return null. This utility allows you to
 * attempt to match codes based on their double value if both are determined to
 * be numeric.
 * 
 * Note that you should not change any code in the code map as the matching will
 * be affected.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class CodeMappingUtility {

	// for reusing instances to save time mapping larger classifications
	private static HashMap<Classification, CodeMappingUtility> instanceMap = new HashMap<>();
	private final HashMap<Double, ArrayList<Code>> codeMap;

	private CodeMappingUtility(Classification classification) {
		codeMap = new HashMap<>();
		for (Code code : classification.getCodeList()) {
			String codeValue;
			if (org.apache.commons.lang3.StringUtils.isNumeric(codeValue = code.getCodeValue())) {
				Double codeDouble = Double.valueOf(codeValue);
				ArrayList<Code> codes = codeMap.get(codeDouble);
				if (codes == null) {
					codes = new ArrayList<>();
					codeMap.put(codeDouble, codes);
				}
				codes.add(code);
			}
		}
	}

	/**
	 * Checks if there is already an instance of the CodeMappingUtility created
	 * for this classification and returns that if so. Otherwise, a fresh
	 * instance is created and put in the instanceMap for possible future use.
	 * 
	 * @param classification
	 * @return instance of the CodeMappingUtility using the provided
	 *         classification
	 */
	public static CodeMappingUtility getInstance(Classification classification) {
		
		CodeMappingUtility instance = instanceMap.get(classification);
		if(instance==null){
			instance = new CodeMappingUtility(classification);
			instanceMap.put(classification, instance);
		}
		return instance;
	}

	/**
	 * Get a list of codes that have the same numeric value as the codeValue
	 * string provided
	 * 
	 * @param codeValue
	 * @return list of matching numeric codes
	 */
	public List<Code> lookupNumericCode(String codeValue) {
		ArrayList<Code> codes = new ArrayList<>();
		if (NumberUtils.isCreatable(codeValue)) {
			ArrayList<Code> matchedCodes = codeMap.get(Double.valueOf(codeValue));
			if (matchedCodes != null) {
				codes.addAll(matchedCodes);
			}
		}
		return codes;
	}

}
