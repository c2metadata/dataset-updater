package us.mtna.dataset.updater;

/**
 * Holds information about a file upload.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class InputInformation {

	private String scriptName;
	private String xmlName;
	private boolean primary;
	private Integer position;

	// xml updater info:
	private String author;
	private String title;
	private String id;

	/**
	 * Name of the dataset as its referred to in the script
	 * 
	 * @return
	 */
	public String getScriptName() {
		return scriptName;
	}

	/**
	 * Name of the dataset as its referred to in the script
	 * 
	 * @return
	 */
	public void setScriptName(String scriptName) {
		this.scriptName = scriptName;
	}

	/**
	 * Name of the dataset as it is in the xml
	 * 
	 * @return
	 */
	public String getXmlName() {
		return xmlName;
	}

	/**
	 * Name of the dataset as it is in the xml
	 * 
	 * @return
	 */
	public void setXmlName(String xmlName) {
		this.xmlName = xmlName;
	}

	/**
	 * Checks if the dataset is set to be the primary dataset. This is the
	 * dataset that will be used if no load command is present. Only one dataset
	 * may be marked as primary per request. If no datasets are found to be
	 * marked as primary, the first dataset received will be used.
	 * 
	 * @return
	 */
	public boolean isPrimary() {
		return primary;
	}

	/**
	 * Set if this is to be used as the primary dataset. This is the dataset
	 * that will be used if no load command is present. Only one dataset may be
	 * marked as primary per request. If no datasets are found to be marked as
	 * primary, the first dataset received will be used.
	 * 
	 * @return
	 */
	public void setPrimary(boolean primary) {
		this.primary = primary;
	}

	/**
	 * The position of the dataset in the xml that this information is
	 * associated with. A null value assumes that the first dataset will be
	 * used. This count is 1-based, so a value of one refers to the first
	 * dataset int the xml
	 * 
	 * @return
	 */
	public Integer getPosition() {
		return position;
	}

	/**
	 * Set the position of the dataset in the xml that this information is
	 * associated with. A null value assumes that the first dataset will be
	 * used. This count is 1-based, so a value of one refers to the first
	 * dataset int the xml
	 * 
	 * @return
	 */
	public void setPosition(Integer position) {
		this.position = position;
	}

	/**
	 * The author to be used in the citation of the newly created xml document.
	 * 
	 * @return
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * Set the author to be used in the newly created xml document. (optional)
	 * 
	 * @param author
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * The title for the newly created xml document.
	 * 
	 * @return
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Set the title for the newly created xml document. (optional)
	 * 
	 * @param title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Get the id used in the citation of the newly created xml document
	 * 
	 * @return
	 */
	public String getId() {
		return id;
	}

	/**
	 * Set the document to be used in the newly created xml document. (optional)
	 * 
	 * @param id
	 */
	public void setId(String id) {
		this.id = id;
	}

}
