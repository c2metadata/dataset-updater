/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.dataset.updater;

import java.util.Collection;
import java.util.LinkedHashMap;

import us.mtna.dataset.updater.exception.UnknownDataSetException;
import us.mtna.pojo.DataSet;

public interface DataSetManager {

	/**
	 * Finds the correct data set from a know collection of data sets based on a
	 * file string that is specified as part of a transformation command. The
	 * file string may not exactly match the file information that is specified
	 * on the data set so implementations of this are expected to find the most
	 * appropriate matches. The data sets returned should not be backed by the
	 * data set held in this manager. Updates should only be made when the data
	 * set is explicitly saved.
	 * 
	 * @param file
	 *            the file string from the transformation command
	 * @return the data set that corresponds to the file string
	 * @throws UnknownDataSetException
	 *             if the data set cannot be determined from the file string
	 */
	DataSet loadDataSet(String file) throws UnknownDataSetException;

	/**
	 * Saves a data set to the manager. This can be an update to an existing
	 * data set or a new instance. No changes to the data sets in this manager
	 * should be persisted until they are explicitly saved with this method.
	 * 
	 * @param dataSet
	 *            the data set to save
	 */
	void saveDataSet(DataSet dataSet);

	/**
	 * Gets the data sets from this manager.
	 * 
	 * @return the data sets in the manager
	 */
	Collection<DataSet> getDataSets();

	/**
	 * set the file matching information on the dataset manager
	 * @param fileMatcher
	 */
	//void setFileMatcher(FileMatcher fileMatcher);
	
	void setDatasets(LinkedHashMap<String, DataSet> datasets);
}
