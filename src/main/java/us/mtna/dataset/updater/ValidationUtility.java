package us.mtna.dataset.updater;

import java.util.ArrayList;

import org.c2metadata.sdtl.pojo.Program;
import org.c2metadata.sdtl.pojo.command.TransformBase;

import us.mtna.data.transform.command.SdtlWrapper;
import us.mtna.data.transform.wrapper.sdtl.ValidationResult;
import us.mtna.data.transform.wrapper.sdtl.WrapperFactory;

/**
 * This validation utility should be run before the dataset updater begins. It
 * assesses all transforms' validity - each wrapper class builds its own
 * validation result and this tool checks the status of all results before
 * continuing. if one or more is false, they will be stored in the
 * invalidTransforms map
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class ValidationUtility {

	/**
	 * called by the constructor. creates wrappers and transform objects, and
	 * analyzes the resulting ValidationResults. Returns the valid boolean,
	 * describing if all transforms in the stack were valid (true), or if one or
	 * more validation results were invalid(false)
	 * 
	 * @return
	 */
	public static ValidationResult validate(Program sdtlProgram) {
		ValidationResult returned = new ValidationResult();
		boolean valid = true;
		ArrayList<String> messages = new ArrayList<>();

		for (TransformBase base : sdtlProgram.getCommands()) {
			SdtlWrapper wrapper = WrapperFactory.wrap(base);
			ValidationResult result = wrapper.validate();

			if (!result.isValid()) {
				valid = false;
				messages.addAll(result.getMessages());
			}
		}

		returned.setValid(valid);
		returned.setMessages(messages);
		return returned;
	}

}
