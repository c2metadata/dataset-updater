package us.mtna.dataset.updater.exception;

import java.util.List;

/**
 * Exception thrown when one or more commands is found to have invalid SDTL.
 * Before the Dataset Updater runs, each sdtl command is checked to make sure it
 * has the properties we need to perform the updates. If a property is found to
 * be invalid or missing, the status of the Validation Result is set to invalid,
 * and a message with details about the invalid property is added.
 * 
 * This Exception contains a list of messages that the consumer of this
 * exception can get and use by calling {{@link #getMessage()}, and there is
 * also a toString method override that will give a formatted list of error
 * messages.
 * 
 * Optionally, use a constructor that includes the language (found on the base
 * sdtl Program object) to specify the source language the sdtl was generated
 * from and alert the relevant parties about issues.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class InvalidSdtlException extends TransformationException {

	private static final long serialVersionUID = 1L;
	private final List<String> messages;
	private String language;

	public InvalidSdtlException(List<String> messages) {
		super();
		this.messages = messages;
	}

	public InvalidSdtlException(List<String> messages, String language) {
		super();
		this.messages = messages;
		this.language = language;
	}

	public InvalidSdtlException(List<String> messages, Throwable cause) {
		super(cause);
		this.messages = messages;
	}

	public InvalidSdtlException(List<String> messages, String language, Throwable cause) {
		super(cause);
		this.messages = messages;
		this.language = language;
	}

	/**
	 * Gets a list of messages about the invalid transform properties.
	 * 
	 * @return
	 */
	public List<String> getMessages() {
		return messages;
	}

	/**
	 * Get the language the script was generated from so that the correct group
	 * may be notified.
	 * 
	 * @return
	 */
	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	/**
	 * Gets a formatted list of messages about the invalid transforms.
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Invalid transforms: ");
		int i = 0;
		for (String message : messages) {
			if (i > 0) {
				sb.append(",\n ");
			}
			sb.append(message);
			i++;
		}
		return sb.toString();
	}

}
