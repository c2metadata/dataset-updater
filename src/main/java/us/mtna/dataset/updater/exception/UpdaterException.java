package us.mtna.dataset.updater.exception;

import us.mtna.reader.exceptions.BaseC2MException;

/**
 * A general exception for errors arising from an issue with the DatasetUpdater,
 * not user input.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class UpdaterException extends BaseC2MException {

	private static final long serialVersionUID = 1L;

	public UpdaterException() {
		super();
	}

	public UpdaterException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public UpdaterException(String message, Throwable cause) {
		super(message, cause);
	}

	public UpdaterException(String message) {
		super(message);
	}

	public UpdaterException(Throwable cause) {
		super(cause);
	}
}
