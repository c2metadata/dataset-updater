package us.mtna.dataset.updater.exception;

import us.mtna.reader.exceptions.RequestException;

public class MissingFilesException extends RequestException{

	private static final long serialVersionUID = 1L;
	private String command;

	public MissingFilesException() {
		super();
	}

	public MissingFilesException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public MissingFilesException(String message, String command) {
		super(message);
		this.command = command;
	}

	public MissingFilesException(Throwable cause) {
		super(cause);
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

}
