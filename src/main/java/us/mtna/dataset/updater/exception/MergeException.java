package us.mtna.dataset.updater.exception;

/**
 * A generic exception for wrapping errors that occur while trying to merge DDI
 * documents.
 * 
 * @author Jack Gager (j.gager@mtna.us)
 *
 */
public class MergeException extends RuntimeException {

	private static final long serialVersionUID = 1615017029993135665L;

	public MergeException(String message) {
		super(message);
	}

	public MergeException(Throwable cause) {
		super("An unexpection error occurred merging the documents", cause);
	}

	public MergeException(String message, Throwable cause) {
		super(message, cause);
	}

}
