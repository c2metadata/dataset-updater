/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.dataset.updater.exception;

import us.mtna.data.transform.command.object.FileDetails;
import us.mtna.dataset.updater.DataSetUpdater;
import us.mtna.reader.exceptions.RequestException;

/**
 * This exception occurs when a transformation refers to data set file that
 * cannot be determined by the {@link DataSetUpdater}.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class UnknownDataSetException extends RequestException {

	private static final long serialVersionUID = 1L;
	private String file;
	// TODO have a constructor that takes a custom file object since you might not know
	// which properties are not null to pass in as the file?

	public UnknownDataSetException(FileDetails customFile){
		super("Tranform file [" + customFile.getFileName() + "] is not known.");
		this.file = customFile.getFileName();
	}
	
	public UnknownDataSetException(String file) {
		super("Tranform file [" + file + "] is not known.");
		this.file = file;
	}

	public String getFile() {
		return file;
	}
}
