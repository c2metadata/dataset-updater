package us.mtna.dataset.updater.exception;
//maybe make this a request exception? 
public class MissingSourceVariableException extends TransformationException {

	private static final long serialVersionUID = 1L;
	private String sourceVariable;
	private String command;

	public MissingSourceVariableException() {
		super();
	}

	public MissingSourceVariableException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public MissingSourceVariableException(String message, Throwable cause) {
		super(message, cause);
	}

	public MissingSourceVariableException(String message, String sourceVariable, String command) {
		super(message);
		this.sourceVariable = sourceVariable;
		this.command = command;
	}

	public MissingSourceVariableException(Throwable cause) {
		super(cause);
	}

	public String getSourceVariable() {
		return sourceVariable;
	}

	public void setSourceVariable(String sourceVariable) {
		this.sourceVariable = sourceVariable;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

}
