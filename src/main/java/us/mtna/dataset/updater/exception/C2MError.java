package us.mtna.dataset.updater.exception;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class C2MError {
	private List<String> errors;
	private String suggestedFix;
	private Integer code;

	public C2MError() {
	}

	public C2MError(List<String> errors) {
		this.errors = errors;
	}

	public C2MError(String suggestedFix, List<String> errors) {
		this.errors = errors;
		this.suggestedFix = suggestedFix;
	}

	public C2MError(String error) {
		this(Collections.singletonList(error));
	}
	
	public C2MError(String suggestedFix, String error) {
		this(Collections.singletonList(error));
		this.suggestedFix = suggestedFix;
	}

	public C2MError(String... errors) {
		this(Arrays.asList(errors));
	}

	public C2MError(Throwable e) {
		this(getErrors(e));
	}

	public Integer getCode() {
		return code;
	}

	public List<String> getErrors() {
		return errors;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}

	public String getSuggestedFix() {
		return suggestedFix;
	}

	public void setSuggestedFix(String suggestedFix) {
		this.suggestedFix = suggestedFix;
	}

	private static List<String> getErrors(Throwable e) {
		List<String> errors = new ArrayList<>();
		do {
			errors.add(e.getMessage());
			e = e.getCause();
		} while (e != null && e.getMessage() != null);
		return errors;
	}
}
