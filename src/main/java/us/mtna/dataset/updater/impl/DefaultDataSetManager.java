/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.dataset.updater.impl;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

import us.mtna.data.transform.command.object.FileDetails;
import us.mtna.dataset.updater.DataSetManager;
import us.mtna.dataset.updater.exception.UnknownDataSetException;
import us.mtna.pojo.DataSet;
import us.mtna.reader.ResourceCopyUtility;

/**
 * The default implementation of the DataSetManager. Uses a linked hashmap to
 * load and save datasets in the DatsetUpdateer.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class DefaultDataSetManager implements DataSetManager {

	private LinkedHashMap<String, DataSet> datasets;

	public DefaultDataSetManager() {
		this.datasets = new LinkedHashMap<>();
	}

	/**
	 * put the current ds and the list of datasets passed in into the map with
	 * the dataset id as the key
	 * 
	 * @param datasets
	 */
	public void mapDatasets(List<DataSet> datasets) {
		for (DataSet dataset : datasets) {
			// this.datasets.put(d.getFile().getFileName(), d);
			// TODO set a script name on ds before it gets here
			if (dataset.getScriptName() == null) {
				this.datasets.put(dataset.getFileDscrName(), dataset);
			} else {
				this.datasets.put(dataset.getScriptName(), dataset);
			}
		}
	}

	/**
	 * use this method if you have a primary dataset already available, to make
	 * sure it gets added to the list
	 * 
	 * @param datasets
	 * @param primaryDataset
	 */
	public void mapDatasets(List<DataSet> datasets, DataSet primaryDataset) {
		// put a copy/snapshot of the primary ds in the list so its current
		// state will be preserved
		this.datasets.put(primaryDataset.getDataSetId(),
				ResourceCopyUtility.copyResource(DataSet.class, primaryDataset));
		for (DataSet dataset : datasets) {
			// this.datasets.put(dataset.getFile().getFileName(), dataset);
			this.datasets.put(dataset.getScriptName(), dataset);
		}
	}

	/**
	 * the dataset returned here will be designated as the primary ds load the
	 * dataset from a file name/id string, stripping extra single parentheses
	 */
	@Override
	public DataSet loadDataSet(String scriptName) {
		if (datasets.size() == 1) {
			return datasets.values().iterator().next();
		}
		// in the script the names are often surrounded with extra single quotes
		scriptName = scriptName.replaceAll("'", "");
		DataSet dataSet;
		if ((dataSet = datasets.get(scriptName)) != null) {
			return dataSet;
		} else {
			dataSet = matchDataSet(scriptName);
		}
		if (dataSet == null) {
			// try and match by index?
			throw new UnknownDataSetException(scriptName);
		}
		return dataSet;
	}

	// don't use file matcher
	public DataSet matchDataSet(String scriptName) throws UnknownDataSetException {
		DataSet matched = null;
		for (DataSet dataset : datasets.values()) {
			if ((dataset.getFileDscrId().trim()).equals(scriptName)
					|| (dataset.getCodebookName().trim()).equals(scriptName)
					|| (dataset.getFileDscrName().trim()).equals(scriptName)) {
				matched = dataset;
			}
		}
		if (matched == null) {
			// try and match by index if scriptname is null?
			throw new UnknownDataSetException(scriptName);
		}
		return matched;
	}

	// TODO this isn't used anywhere
	/**
	 * pass a customFile object here and all fields will be checked for a key
	 * match to return a ds.
	 * 
	 * @param file
	 * @return dataset with a key that matches any of the fields of the custom
	 *         file object.
	 * @throws UnknownDataSetException
	 */
	public DataSet loadDataSet(FileDetails file) throws UnknownDataSetException {

		if (datasets.get(file.getFileName()) != null) {
			return datasets.get(file.getFileName());
		} else if (datasets.get(file.getFilePath()) != null) {
			return datasets.get(file.getFilePath());
		} else if (datasets.get(file.getFileNameAndExtension()) != null) {
			return datasets.get(file.getFileNameAndExtension());
		} else if (datasets.get(file.getExtension()) != null) {
			return datasets.get(file.getExtension());
		} else if (datasets.get(file.getUri()) != null) {
			return datasets.get(file.getUri());
		} else {
			throw new UnknownDataSetException(file.toString());
		}
	}

	/**
	 * save a copy of the primary dataset passed in so that you'll have a
	 * snapshot of its current state if the primary dataset gets changed
	 * afterward
	 */
	@Override
	public void saveDataSet(DataSet dataSet) {
		DataSet copydataSet = ResourceCopyUtility.copyResource(DataSet.class, dataSet);
		datasets.put(copydataSet.getDataSetId(), copydataSet);
	}

	/**
	 * gets the list of all stored datasets
	 */
	@Override
	public Collection<DataSet> getDataSets() {
		return datasets.values();
	}

	/**
	 * for use in joinDatasets and maybe elsewhere, load
	 * 
	 * @param id
	 * @return dataset with the given id
	 */
	public DataSet getDataSet(String id) {
		return datasets.get(id);
	}

	public void setDatasets(LinkedHashMap<String, DataSet> datasets) {
		this.datasets = datasets;
	}

}
