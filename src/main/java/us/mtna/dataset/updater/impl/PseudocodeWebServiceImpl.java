package us.mtna.dataset.updater.impl;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import us.mtna.transform.cogs.json.PseudocodeService;

/**
 * Wrapper for the ICPSR Pseudocode Service, this class just makes a call to the
 * /text endpoint to get the text representation
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class PseudocodeWebServiceImpl implements PseudocodeService {

	private String path;
	private String host;
	
	public PseudocodeWebServiceImpl() {
		this.host = "localhost:8080";
		this.path = "/pseudocodeservice-web/pseudocode/command/text";
	}
	
	@Override
	public String generate(String command) {

		try {
			RestTemplate rest = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
			MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
			map.add("command", command);
			HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map,
					headers);
			ResponseEntity<String> response = rest.postForEntity(host+path, request, String.class);
			// String returned = rest.postForObject(url + "/text", command,
			// String.class);
			if(response.getBody()==null){
				return "Unable to retreive pseudocode.";
			}
			return response.getBody();
		} catch (Exception e) {
			return "Unable to retreive pseudocode.";
		}
	}
	
	public String getPath() {
		return path;
	}
	
	
	public void setPath(String path) {
		this.path = path;
	}

	public String getHost() {
		return host;
	}
	
	public void setHost(String host) {
		this.host = host;
	}
}
