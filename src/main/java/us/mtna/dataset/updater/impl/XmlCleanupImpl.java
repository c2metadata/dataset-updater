package us.mtna.dataset.updater.impl;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.annotation.processing.Processor;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;

import org.xml.sax.XMLReader;

import net.sf.saxon.Transform;

/**
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class XmlCleanupImpl {

	//this one works but only with a file path
//	public InputStream cleanUp(InputStream xml) {
//
//		String[] arglist = { "-o:target/outputfile.xml", "src/test/resources/inputDdi/hhincomeInputDdi.xml",
//				"target/transform.xsl" };
//		InputStream targetStream;
//		try {
//			Transform.main(arglist);
//			// return the outputfile as an output stream.
//			File output = new File("target/outputFile.xml");
//			targetStream = new FileInputStream(output);
//			// return target stream or write bytes to output stream
////			Path path = Paths.get("target/outputFile.xml");
////			byte[] bytes = Files.readAllBytes(path);
////			ByteArrayOutputStream bos = new ByteArrayOutputStream();
//		} catch (FileNotFoundException e) {
//			throw new RuntimeException("Couldn't find outputFile", e);
//		} catch (Exception e) {
//			throw new RuntimeException("Error transforming DDI file to DDI2.5", e);
//		}
//		return targetStream;
//	}
//	
//	//trying to use stream source
//	public InputStream transform(InputStream xml) {
//		StreamSource ss = new StreamSource();
//		ss.setInputStream(xml);
//		TransformerFactory tFactory = TransformerFactory.newInstance();
//		Transformer transformer = tFactory
//		          .newTransformer(ss);
//		transformer.transform(new StreamSource(new StringReader(xml)), null));
//		transformer.transform(xml, new Result());
//		String[] arglist = { "-o:target/outputfile.xml", ss.getInputStream(),
//		"target/transform.xsl" };
//		InputStream targetStream;
//		try {
//
//			Transform.main(arglist);
//			// return the outputfile as an output stream.
//			File output = new File("target/outputFile.xml");
//			targetStream = new FileInputStream(output);
//			// return target stream or write bytes to output stream
//			Path path = Paths.get("target/outputFile.xml");
//			byte[] bytes = Files.readAllBytes(path);
//			ByteArrayOutputStream bos = new ByteArrayOutputStream();
//
//		} catch (FileNotFoundException e) {
//			throw new RuntimeException("Couldn't find outputFile", e);
//
//		} catch (Exception e) {
//			throw new RuntimeException("Error transforming DDI file to DDI2.5", e);
//		}
//
//		return targetStream;
//	}
//	
//	public OutputStream getOutputStream(InputStream xml) {
//		String[] arglist = { "-o:target/outputfile.xml", xml.toString(), "target/transform.xsl" };
//		ByteArrayOutputStream bos = new ByteArrayOutputStream();
//		try {
//			StreamSource ss = new StreamSource();
//			ss.setInputStream(xml);
//			Transform.main(arglist);
//			
//			
//			Path path = Paths.get("target/outputFile.xml");
//			byte[] bytes = Files.readAllBytes(path);
//			// bytes = bos.toByteArray();
//			bos.write(bytes);
//		} catch (FileNotFoundException e) {
//			throw new RuntimeException("Couldn't find outputFile", e);
//
//		} catch (Exception e) {
//			throw new RuntimeException("Error transforming DDI file to DDI2.5", e);
//		}
//
//		return bos;
//	}
//
//	// JUST FOR TESTING AND PRINTING TO CONSOLE, DELETE AFTER
//	// convert InputStream to String
//	private static String getStringFromInputStream(InputStream is) {
//
//		BufferedReader br = null;
//		StringBuilder sb = new StringBuilder();
//
//		String line;
//		try {
//
//			br = new BufferedReader(new InputStreamReader(is));
//			while ((line = br.readLine()) != null) {
//				sb.append(line);
//			}
//
//		} catch (IOException e) {
//			e.printStackTrace();
//		} finally {
//			if (br != null) {
//				try {
//					br.close();
//				} catch (IOException e) {
//					e.printStackTrace();
//				}
//			}
//		}
//		return sb.toString();
//	}



}
