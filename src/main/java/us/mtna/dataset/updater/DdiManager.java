package us.mtna.dataset.updater;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;
import org.ddialliance.ddi_2_5.xml.xmlbeans.CitationType;
import org.ddialliance.ddi_2_5.xml.xmlbeans.CodeBookDocument;
import org.ddialliance.ddi_2_5.xml.xmlbeans.CodeBookType;
import org.ddialliance.ddi_2_5.xml.xmlbeans.DataDscrType;
import org.ddialliance.ddi_2_5.xml.xmlbeans.DocDscrType;
import org.ddialliance.ddi_2_5.xml.xmlbeans.FileDscrType;
import org.ddialliance.ddi_2_5.xml.xmlbeans.MaterialReferenceType;
import org.ddialliance.ddi_2_5.xml.xmlbeans.OtherMatType;
import org.ddialliance.ddi_2_5.xml.xmlbeans.OthrStdyMatType;
import org.ddialliance.ddi_2_5.xml.xmlbeans.ProdStmtType;
import org.ddialliance.ddi_2_5.xml.xmlbeans.RspStmtType;
import org.ddialliance.ddi_2_5.xml.xmlbeans.StdyDscrType;
import org.ddialliance.ddi_2_5.xml.xmlbeans.TitlStmtType;
import org.ddialliance.ddi_2_5.xml.xmlbeans.VerStmtType;
import org.openmetadata.util.xmlbeans.XhtmlUtilities;
import org.openmetadata.util.xmlbeans.XmlIdRandomizer;
import org.slf4j.LoggerFactory;

import us.mtna.dataset.updater.exception.MergeException;
import us.mtna.updater.DatasetMetadata;

/**
 * A utility to merge two DDI files into one, making sure that XML ids are not
 * conflicting.
 * 
 * Instances may be initialized either by passing in values to be used in the
 * newly created Citation, or with an empty constructor, which will use the
 * default values where required.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class DdiManager {

	private String author;
	private String title;
	private String date;
	private String id;
	private static final String VERSION = "V1";
	private static final String NOTE = "Data from this study were used in the transformations described in this DDI Instance ";
	private static final String DEFAULT_TITLE = "Generated with the C2Metadata Dataset Updater Service.";
	private static org.slf4j.Logger log = LoggerFactory.getLogger(DdiManager.class);

	private DatasetMetadata metadata;

	// pass in cofig object or just the properties used?
	// passing in just the strings for now since not sure if it would create a
	// circular dependency
	public DdiManager(String author, String title, String date, String id) {
		this.author = author;
		this.title = title;
		this.date = date;
		this.id = id;
	}

	public DdiManager(DatasetMetadata metadata) {
		this.metadata = metadata;
	}

	public DdiManager() {
		// can also initialize with this empty constructor, default values will
		// be used.
	}

	/**
	 * Merges input streams of DDI 2.5 documents into a single XML Beans DDI 2.5
	 * {@link CodeBookDocument}.
	 * 
	 * @param inputStreams
	 *            the input streams to merge
	 * @return the merged XML document
	 * @throws MergeException
	 *             if an error occurs during the merge
	 */
	//TODO remove this in favor of the one below
	public CodeBookDocument merge(InputStream... inputStreams) throws MergeException {
		// parse each file into an xml object.
		List<CodeBookDocument> cbDocuments = new ArrayList<>();
		for (InputStream input : inputStreams) {
			CodeBookDocument codeBook;
			try {
				codeBook = CodeBookDocument.Factory.parse(input);
			} catch (XmlException | IOException e) {
				log.error("An error occured reading the streams to merge");
				throw new MergeException("An error occured reading the streams to merge", e);
			}
			cbDocuments.add(codeBook);
		}
		return merge(cbDocuments);
	}

	public CodeBookDocument merge(XmlObject... inputStreams) throws MergeException {
		// parse each file into an xml object.
		List<CodeBookDocument> cbDocuments = new ArrayList<>();
		for (XmlObject input : inputStreams) {
			cbDocuments.add((CodeBookDocument) input);
		}
		return merge(cbDocuments);
	}

	/**
	 * Merges files of DDI 2.5 documents into a single XML Beans DDI 2.5
	 * {@link CodeBookDocument}.
	 * 
	 * @param files
	 *            the files to merge
	 * @return the merged XML document
	 * @throws MergeException
	 *             if an error occurs during the merge
	 */
	public CodeBookDocument merge(File... files) throws MergeException {
		// parse each file into an xml object.
		List<CodeBookDocument> cbDocuments = new ArrayList<>();
		for (File file : files) {
			CodeBookDocument codeBook;
			try {
				codeBook = CodeBookDocument.Factory.parse(file);
			} catch (XmlException | IOException e) {
				log.error("An error occured reading the files to merge", e);
				throw new MergeException("An error occured reading the files to merge", e);
			}
			cbDocuments.add(codeBook);
		}
		return merge(cbDocuments);
	}

	private CodeBookDocument merge(Collection<CodeBookDocument> documents) {
		try {
			// holds ids that are used in the merged document
			HashSet<String> ids = new HashSet<>();

			// Create a new CodeBookDocument with a CodebookType to merge the
			// files into. Manually set an id on it.
			CodeBookDocument mergedCodeDoc = CodeBookDocument.Factory.newInstance();
			CodeBookType cbt = mergedCodeDoc.addNewCodeBook();
			cbt.setID("MERGED_DDI");

			// Iterate through each input DDI XmlObject, call
			// XmlIdRandomizer.randomizeIds(), passing the DDI XmlObject and the
			// id set
			for (CodeBookDocument cb : documents) {
				log.info("merging codebook [" + cb.getCodeBook().getID() + "] into the merged document");
				XmlIdRandomizer.randomizeIds(cb, ids);
				copyElementsIntoNewDoc(cb, mergedCodeDoc);
			}

			// add the citations to the new document
			addCitations(documents, mergedCodeDoc);

			return mergedCodeDoc;
		} catch (RuntimeException e) {
			log.error("Merge exception when trying to merge codebooks into a single document");
			throw new MergeException(e);
		}
	}

	/**
	 * Copy over the citations from the previous document(s) into a new related
	 * study section, and create a new citation element describing the new
	 * document.
	 * 
	 * @param documents
	 * @param newDoc
	 */
	// FIXME: moved this to the xml updater, so either remove it from here or
	// from there.
	// We do need this for merging in multiple docs into the new doc though. how
	// to keep these in the xml updater? preserve the citations and just add to
	// them?
	private void addCitations(Collection<CodeBookDocument> documents, CodeBookDocument newDoc) {

		StdyDscrType newDocStdy = newDoc.getCodeBook().getStdyDscrList().get(0);

		// add the citations from the other documents in the "related studies"
		// section
		OthrStdyMatType other = newDocStdy.addNewOthrStdyMat();
		for (CodeBookDocument cbd : documents) {
			List<CitationType> citations = new ArrayList<>();
			MaterialReferenceType relStdy = other.addNewRelStdy();
			for (StdyDscrType sdt : cbd.getCodeBook().getStdyDscrList()) {
				for (CitationType citation : sdt.getCitationList()) {
					citations.add(citation);
				}
				relStdy.setCitationArray(citations.toArray(new CitationType[0]));
			}
		}

		// add the citation describing the newly created file using configurable
		// fields

		CitationType citation = newDocStdy.addNewCitation();
		TitlStmtType titleType = citation.addNewTitlStmt();
		// citation title is mandatory
		if (title == null || title.isEmpty()) {
			title = DEFAULT_TITLE;
		}
		XhtmlUtilities.setXhtmlContent(titleType.addNewTitl(), title);

		if (id != null && !id.isEmpty()) {
			XhtmlUtilities.setXhtmlContent(titleType.addNewIDNo(), id);
		}

		// need rspStmt? verStmt? holdings URI?
		RspStmtType rsp = citation.addNewRspStmt();
		ProdStmtType prod = citation.addNewProdStmt();

		// not sure which of these to use
		if (author != null && !author.isEmpty()) {
			XhtmlUtilities.setXhtmlContent(rsp.addNewAuthEnty(), author);
			XhtmlUtilities.setXhtmlContent(prod.addNewProducer(), author);
		}
		if (date != null && !date.isEmpty()) {
			XhtmlUtilities.setXhtmlContent(prod.addNewProdDate(), date);
		}
		VerStmtType ver = citation.addNewVerStmt();
		if (VERSION != null && !VERSION.isEmpty()) {
			XhtmlUtilities.setXhtmlContent(ver.addNewVersion(), VERSION);
		}
		// this is only in the verStmts describing the existing studies, not for
		// the newly created verstmts.
		if (NOTE != null && !NOTE.isEmpty()) {
			XhtmlUtilities.setXhtmlContent(ver.addNewNotes(), NOTE);
		}
	}

	/**
	 * Create a fresh codebook document with the appropriate elements
	 * transferred over from the original.
	 * 
	 * @param originalCodeBook
	 */
	public CodeBookDocument createNewDocument(CodeBookDocument originalCodeBook) {
		CodeBookDocument newDocument = CodeBookDocument.Factory.newInstance();
		newDocument.addNewCodeBook();
		copyElementsIntoNewDoc(originalCodeBook, newDocument);
		// instead, add them in the xml updater since its creating a new doc
		// anyway.
		// addCitations(Arrays.asList(originalCodeBook), newDocument);
		return newDocument;
	}

	/**
	 * Add the contents of the original DDI XmlObject to the new DDI document
	 * you created. Return the newly edited codebook.
	 * 
	 * @param originalCodeBook
	 * @param newCodeDoc
	 */
	private void copyElementsIntoNewDoc(CodeBookDocument originalCodeBook, CodeBookDocument newCodeDoc) {
		CodeBookType cbType = originalCodeBook.getCodeBook();
		CodeBookType mergedCD = newCodeDoc.getCodeBook();

		for (StdyDscrType sd : cbType.getStdyDscrList()) {
			StdyDscrType newStdy = mergedCD.addNewStdyDscr();
			newStdy.set(sd);
		}

		for (DocDscrType sd : cbType.getDocDscrList()) {
			DocDscrType newStdy = mergedCD.addNewDocDscr();
			newStdy.set(sd);
		}

		for (DataDscrType sd : cbType.getDataDscrList()) {
			DataDscrType newStdy = mergedCD.addNewDataDscr();
			newStdy.set(sd);
		}

		for (FileDscrType sd : cbType.getFileDscrList()) {
			FileDscrType newStdy = mergedCD.addNewFileDscr();
			newStdy.set(sd);
		}

		for (OtherMatType sd : cbType.getOtherMatList()) {
			OtherMatType newStdy = mergedCD.addNewOtherMat();
			newStdy.set(sd);
		}

	}

	public static org.slf4j.Logger getLog() {
		return log;
	}

	public static void setLog(org.slf4j.Logger log) {
		DdiManager.log = log;
	}

}
