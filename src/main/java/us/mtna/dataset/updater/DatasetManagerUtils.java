package us.mtna.dataset.updater;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.apache.xmlbeans.XmlError;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;
import org.apache.xmlbeans.XmlOptions;
import org.ddialliance.ddi_2_5.xml.xmlbeans.CodeBookDocument;
import org.ecoinformatics.eml_2_1_1.xml.xmlbeans.EmlDocument;
import org.springframework.web.multipart.MultipartFile;

import us.mtna.c2metadata.xml.Ddi25XmlUpdater;
import us.mtna.c2metadata.xml.EmlUpdater;
import us.mtna.c2metadata.xml.XmlUpdater;
import us.mtna.reader.Reader;
import us.mtna.reader.ReaderImplDDI25;
import us.mtna.reader.ReaderImplEML211;
import us.mtna.reader.exceptions.ReaderException;
import us.mtna.reader.exceptions.RequestException;
import us.mtna.reader.exceptions.XmlValidationException;

/**
 * Utility methods used across the dataset updater and managers
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class DatasetManagerUtils {

	/**
	 * get an instance of the correct reader based on the xml type
	 * 
	 * @param xmlObj
	 * @return
	 */
	public static Reader getReaderImpl(XmlObject xmlObj) {

		Reader reader = null;
		if (xmlObj instanceof CodeBookDocument) {
			reader = new ReaderImplDDI25();
		} else if (xmlObj instanceof EmlDocument) {
			reader = new ReaderImplEML211();
		} else {
			throw new RequestException("XML type not recognized");
		}
		return reader;
	}
	
	public static XmlUpdater getUpdaterImpl(XmlObject xmlObj) {

		XmlUpdater reader = null;
		if (xmlObj instanceof CodeBookDocument) {
			reader = new Ddi25XmlUpdater();
		} else if (xmlObj instanceof EmlDocument) {
			reader = new EmlUpdater();
		} else {
			throw new RequestException("XML type not recognized");
		}
		return reader;
	}

	public static XmlObject parseInputStream(InputStream xml) {
		try {
			return XmlObject.Factory.parse(xml);
//			return XmlObject.Factory.parse(xml, new XmlOptions().setLoadStripWhitespace());
		} catch (XmlException | IOException e) {
			throw new ReaderException("Error reading input stream as XML ", e);
		}
	}
	
	public static XmlObject parseInputStream(MultipartFile xml) {
		try {
			return XmlObject.Factory.parse(xml.getInputStream());
//			return XmlObject.Factory.parse(xml, new XmlOptions().setLoadStripWhitespace());
		} catch (XmlException | IOException e) {
			throw new ReaderException("Error reading input stream as XML ", e);
		}
	}


	public static void validate(XmlObject xml) {
		ArrayList<XmlError> errors = new ArrayList<>();
		if (!xml.validate(new XmlOptions().setErrorListener(errors))) {
			errors.get(0).getMessage();
		}
		if (!errors.isEmpty()) {
			throw new XmlValidationException(errors);
		}
	}

}
