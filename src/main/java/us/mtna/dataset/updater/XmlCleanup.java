package us.mtna.dataset.updater;

import java.io.InputStream;
import java.io.OutputStream;


/**
 * Utility to convert files to DDI2.5 using xsl transform
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public interface XmlCleanup {

	OutputStream transformToDdi25(InputStream xml);
}