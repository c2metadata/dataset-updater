package us.mtna.dataset.updater;

import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;

import org.apache.xmlbeans.XmlObject;
import org.apache.xmlbeans.XmlOptions;
import org.c2metadata.sdtl.pojo.Program;
import org.c2metadata.sdtl.pojo.command.TransformBase;
import org.ddialliance.ddi_2_5.xml.xmlbeans.CodeBookDocument;
import org.ecoinformatics.eml_2_1_1.xml.xmlbeans.EmlDocument;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import us.mtna.c2metadata.xml.Ddi25XmlUpdater;
import us.mtna.c2metadata.xml.XmlUpdater;
import us.mtna.data.transform.command.SdtlWrapper;
import us.mtna.data.transform.wrapper.sdtl.ValidationResult;
import us.mtna.data.transform.wrapper.sdtl.WrapperFactory;
import us.mtna.dataset.updater.exception.InvalidSdtlException;
import us.mtna.dataset.updater.impl.PseudocodeWebServiceImpl;
import us.mtna.pojo.DataSet;
import us.mtna.pojo.Transform;
import us.mtna.pojo.file.matching.FileMatcher;
import us.mtna.reader.Reader;
import us.mtna.reader.ReaderImplDDI25;
import us.mtna.reader.exceptions.RequestException;
import us.mtna.transform.cogs.json.PseudocodeService;
import us.mtna.transform.cogs.json.TransformMapper;
import us.mtna.updater.Author;
import us.mtna.updater.DatasetMetadata;

/**
 * Implementation of {@link OutputGenerator} that gets the updated XML by
 * parsing the datasets using the {@link ReaderImplDDI25}, then taking those
 * datasets and the Program POJO passed in and updating the dataset in the
 * {@link DatasetUpdater}. Then pass that updated dataset and the original xml
 * to the {@link Ddi25XmlUpdater}
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class Ddi25OutputGenerator {

	private static org.slf4j.Logger log = LoggerFactory.getLogger(Ddi25OutputGenerator.class);
	private boolean validate;
	private String pseudocodeHost;

	public Ddi25OutputGenerator() {
		this.validate = true;
	}

	/**
	 * Updates the provided DDI based on the provided SDTL transformations and
	 * returns the value new DDI as a string.
	 * 
	 * @param sdtl
	 *            the SDTL transformations to apply
	 * @param xml
	 *            the original DDI
	 * @return the updated DDI as a string
	 */
	public String getUpdatedXmlAsString(Program sdtl, InputStream xml) {
		return produceDDI(sdtl, new DdiManager().createNewDocument(parse(xml))).xmlText();
	}

	@Deprecated
	public String getUpdatedXmlAsString(Program sdtl, InputStream xml, FileMatcher matcher) {
		return produceDDI(sdtl, new DdiManager().createNewDocument(parse(xml))).xmlText();
	}

	/**
	 * This is the method used by the dataset-updater-ws API and tests. All of the work of the 
	 * 
	 * @param fileInformation
	 * @param program
	 * @param datasets
	 * @param xmlObject
	 * @return
	 */
	public String getUpdatedXmlAsString(InputInformation[] fileInformation, Program program, List<DataSet> datasets,
			XmlObject xmlObject) {

		return produceDDIWithDsMap(fileInformation, program, datasets, xmlObject)
				.xmlText(new XmlOptions().setLoadStripWhitespace());
	}

	/**
	 * Updates the provided DDI based on the provided SDTL transformations and
	 * returns the value new DDI as an input stream.
	 * 
	 * @param sdtl
	 *            the SDTL transformations to apply
	 * @param xml
	 *            the original DDI
	 * @return the updated DDI as an input stream
	 */
	public InputStream getUpdatedXmlAsStream(Program sdtl, InputStream xml) {
		// FIXME is this createNewDocument necessary?
		return produceDDI(sdtl, new DdiManager().createNewDocument(parse(xml))).newInputStream();
	}

	@Deprecated
	public InputStream getUpdatedXmlAsStream(Program sdtl, InputStream xml, FileMatcher matcher) {
		return produceDDI(sdtl, new DdiManager().createNewDocument(parse(xml))).newInputStream();
	}

	/**
	 * Merges the provided DDI documents and produces a single DDI document and
	 * updates the merged DDI based on the provided SDTL transformations and
	 * returns the value new DDI as a string.
	 * 
	 * @param sdtl
	 *            the SDTL transformations to apply
	 * @param xml
	 *            the original DDI files
	 * @return the updated DDI as a string
	 */
	public String getUpdatedXmlAsString(Program sdtl, InputStream... xml) {
		return produceDDIMultiDocumemnt(sdtl, xml).xmlText();
	}

	@Deprecated
	public String getUpdatedXmlAsString(Program sdtl, FileMatcher matcher, InputStream... xml) {
		return produceDDIMultiDocumemnt(sdtl, xml).xmlText();
	}

	/**
	 * Merges the provided DDI documents and produces a single DDI document and
	 * updates the merged DDI based on the provided SDTL transformations and
	 * returns the value new DDI as an input stream.
	 * 
	 * @param sdtl
	 *            the SDTL transformations to apply
	 * @param xml
	 *            the original DDI files
	 * @return the updated DDI as an input stream
	 */
	public InputStream getUpdatedXmlAsStream(Program sdtl, InputStream... xml) {
		return produceDDIMultiDocumemnt(sdtl, xml).newInputStream();
	}

	@Deprecated
	public InputStream getUpdatedXmlAsStream(Program sdtl, FileMatcher matcher, InputStream... xml) {
		return produceDDIMultiDocumemnt(sdtl, xml).newInputStream();
	}

	protected XmlObject produceDDIMultiDocumemnt(Program sdtlProgram, InputStream... inputs) {
		List<XmlObject> objects = new LinkedList<>();
		for (InputStream stream : inputs) {
			objects.add(DatasetManagerUtils.parseInputStream(stream));
		}
		XmlObject xmlObject = objects.get(0);
		if (inputs.length > 1) {
			// determine the right xmlObject implementation then merge
			if (xmlObject instanceof CodeBookDocument) {
				return produceDDI(sdtlProgram, new DdiManager().merge(objects.toArray(new XmlObject[0])));
			} else if (xmlObject instanceof EmlDocument) {
				throw new RequestException(
						"More than one EML document was found, and EML merging is not yet available. Please consolidate your datasets into one file if possible.");
			}
		} else if (inputs.length == 1) {
			// don't merge - parse into the correct xml document
			return produceDDI(sdtlProgram, xmlObject);
		} else {
			throw new RequestException("No XML found");
		}
		return null;
	}

	protected CodeBookDocument parse(InputStream xml) {
		XmlObject xmlObject = DatasetManagerUtils.parseInputStream(xml);
		DatasetManagerUtils.validate(xmlObject);
		// FIXME this may not be a codebook document
		return (CodeBookDocument) xmlObject;
	}

	/**
	 * method to produce DDI from a single codebookDocument.
	 * 
	 * @param sdtlProgram
	 * @param xml
	 *            the xml document that will be updated by the XML Updater
	 * @return the final updated DDI XML document.
	 */
	protected XmlObject produceDDI(Program sdtlProgram, XmlObject xml) {
		log.trace("Preparing to read xml into dataset objects");
		Reader reader = DatasetManagerUtils.getReaderImpl(xml);
		List<DataSet> readerDataSets = reader.getDataSets(xml);
		return produceDDIWithDsMap(null, sdtlProgram, readerDataSets, xml);
	}

	protected XmlObject produceDDIWithDsMap(InputInformation[] fileInformation, Program program, List<DataSet> datasets,
			XmlObject originalXml) {
		log.trace("Preparing to read xml into dataset objects");
		// primary dataset was moved to index 0 in the updater manager
		DataSet primary = datasets.get(0);
		DataSetUpdater datasetUpdater = new DataSetUpdater(primary, datasets);

		if (validate) {
			ValidationResult result = ValidationUtility.validate(program);
			// validate will throw exception and exit if needed.
			if (!result.isValid()) {
				String language = program.getSourceLanguage() != null ? program.getSourceLanguage() : "";
				throw new InvalidSdtlException(result.getMessages(), language);
			}
		}
		ObjectMapper mapper = new ObjectMapper();
		PseudocodeWebServiceImpl pseudocodeService = new PseudocodeWebServiceImpl();
		if (pseudocodeHost != null) {
			pseudocodeService.setHost(pseudocodeHost);
		}
		for (TransformBase base : program.getCommands()) {
			log.trace("preparing to update with command " + (base.getSourceInformation() != null
					? base.getSourceInformation().getOriginalSourceText() : "[no source provided]"));
			SdtlWrapper wrapper = WrapperFactory.wrap(base);
			String command;
			try {
				command = pseudocodeService.generate(mapper.writeValueAsString(base));
			} catch (JsonProcessingException e) {
				command = "Unable to retreive pseudocode.";
			}
			Transform transform = TransformMapper.mapTransformBase(base, program.getSourceLanguage(), command);
			datasetUpdater.updateDataSet(wrapper, transform);
		}
		XmlUpdater updater = DatasetManagerUtils.getUpdaterImpl(originalXml);
		updater.setLog(log);
		updater.setDatasetMetadata(populateDatasetMetadata(fileInformation));
		return updater.updateXml(originalXml, primary);
	}

	/**
	 * Check all the input information objects for dataset metadata information
	 * to populate a dataset metadata object.
	 * 
	 * @param fileInformation
	 * @return
	 */
	private DatasetMetadata populateDatasetMetadata(InputInformation[] fileInformation) {
		DatasetMetadata metadata = new DatasetMetadata();
		if (fileInformation != null) {
			for (InputInformation info : fileInformation) {
				if (info != null) {
					if (info.getTitle() != null) {
						metadata.setDocumentTitle(info.getTitle());
					}
					if (info.getAuthor() != null) {
						Author author = new Author();
						author.setFirstName(info.getAuthor());
						metadata.getAuthors().add(author);
					}
				}
			}
		}
		return metadata;
	}

	/**
	 * Used to set the pseudocode service for generating pseudocode from the
	 * SDTL.
	 * 
	 * @param service
	 *            PseudocodeService implementation
	 */
	public void setPseudocodeService(PseudocodeService service) {
		TransformMapper.setPseudocodeService(service);
	}

	/**
	 * Set whether the validation process should be run before the updating the
	 * dataset. It is set to run by default. Setting validate to false will skip
	 * the validation process.
	 * 
	 * @param validate
	 */
	public void setValidation(boolean validate) {
		this.validate = validate;
	}

	public void setPseudocodeHost(String pseudocodeHost) {
		this.pseudocodeHost = pseudocodeHost;
	}

	public String getPseudocodeHost() {
		return pseudocodeHost;
	}

	public static org.slf4j.Logger getLog() {
		return log;
	}

	public static void setLog(org.slf4j.Logger log) {
		Ddi25OutputGenerator.log = log;
	}
}
